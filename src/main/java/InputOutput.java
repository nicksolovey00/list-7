import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import house.House;

import java.io.*;
import java.util.ArrayList;

public class InputOutput {
    /* Филиппов А.В. 30.11.2020 Комментарий не удалять.
     Не работает.
     Предполагается наличие двух функций. Одна записывает в двоичном виде в поток, вторая читает.
     void writeBinaryData(OutputStream s, int[] d);
     void readBinaryData(InputStream s, int[] d);
     Читаем в уже созданный массив.

     Реализация в принципе правильная. Нужно разбить и переписать тест.
    */
    public static void writeBinaryData(OutputStream s, int[] d){
        try(DataOutputStream dos = new DataOutputStream(s)){
            for(int i: d){
                dos.writeInt(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readBinaryData(InputStream s, int[] d){
        try(DataInputStream dis = new DataInputStream(s)) {
            for(int i = 0; i < d.length; i++){
                d[i] = dis.readInt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* Филиппов А.В. 30.11.2020 Комментарий не удалять.
     Не работает.
     Разбить на две функции см. выше.
     Плюс сломал тест. На выходе должен получаться текст содержаший числа.
    */

    public static void write(Writer writer, int[] array) throws IOException {
        for (int i: array){
            writer.write(i + " ");
        }
    }

    public static void read(Reader reader, int[] array) throws IOException {
        StringBuilder sb = new StringBuilder();

        while(reader.ready()){
            int character = reader.read();
            if (character == -1) {
                break;
            }
            sb.append((char) character);
        }
        String[] arrayParseInt = sb.toString().split("\\s");
        for (int i = 0; i < array.length; ++i) {
            array[i] = Integer.parseInt(arrayParseInt[i]);
        }
    }

    public static void writeArrayToFile(int[] array){
        try (RandomAccessFile raf = new RandomAccessFile(".\\Test\\test4.txt", "rw")){
            for (int i: array){
                raf.writeInt(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int[] readArrayFromFileFromPos(int pos){
        int[] res = new int[0];
        try (RandomAccessFile raf = new RandomAccessFile(".\\Test\\test2.txt", "r")){
            res = new int[(int)raf.length()/4 - pos];
            raf.seek(4*pos);

            for (int i = 0; i < res.length; i++){
                res[i] = raf.readInt();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static File[] getFilesWithResolution(String path, String resolution) throws LogicException {
        File file = new File(path);
        if(!file.isDirectory()){
            throw new LogicException("Not a directory");
        }

        ArrayList<File> res = new ArrayList<>();
        File[] temp = file.listFiles();
        for (File f: temp){
            if(f.toString().endsWith(resolution) && !f.isDirectory()){
                res.add(f);
            }
        }

        return res.toArray(new File[0]);
    }

    public static void serializeHouse(House house, OutputStream os){
        try(ObjectOutputStream oos = new ObjectOutputStream(os)) {
            oos.writeObject(house);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static House deserializeHouse(InputStream is){
        House house = null;
        try(ObjectInputStream ois = new ObjectInputStream(is)) {
            house = (House) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return house;
    }

    /* Филиппов А.В. 30.11.2020 Комментарий не удалять.
     Не работает.
     gson и jackson - это две разные библиотеки.
     В задании написан jackson.
    */
    public static String serializeHouseToJacksonString(House house) {
        try {
            return new ObjectMapper().writeValueAsString(house);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public static House deserializeHouseFromJacksonString(String json) {
        try {
            return new ObjectMapper().readValue(json, House.class);
        } catch (IOException e) {
            return null;
        }
    }
}

