package house;

import java.io.Serializable;
import java.util.Objects;

public class Date implements Serializable {
    private int year;
    private int month;
    private int day;

    public Date() {
    }

    public Date(int year, int month, int day) throws DateException {
        setYear(year);
        setMonth(month);
        setDay(day);
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) throws DateException {
        if(year < 0){
            throw new DateException("Wrong year input");
        }
        this.year = year;
    }

    public int getMonth(){
        return month;
    }

    public void setMonth(int month) throws DateException {
        if(month > 12 || month < 1){
            throw new DateException("Wrong month input");
        }
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) throws DateException {
        if(day > 31 || day < 1){
            throw new DateException("Wrong day input");
        }
        this.day = day;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Date date = (Date) o;
        return year == date.year &&
                month == date.month &&
                day == date.day;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, day);
    }
}

